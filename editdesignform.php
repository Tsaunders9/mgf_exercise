<form id="overlay-form" class="validate-form" method="post" action="adddesign.php">
                                <div class="form-group">
                                    <label class="">Design Title</label>
                                    <div>
                                        <input name="title" class="validate[required] form-control" placeholder="Everything has proceded according to my design.">
                                    </div>
                                </div>
								<div class="form-group">
                                    <label class="">Design Type</label>
									
									 <select name="type">
									 <?php
									 
									 include "common/databasedeets.php";
									 
									 	$sql = "SELECT * FROM design_types";
										$result = $conn->query($sql);
										
										if ($result->num_rows > 0) {
										
											while($row = $result->fetch_assoc()) {
												$type = $row["id"];
												$desc = $row["description"];
												echo "<option value='$type'>$desc</option>";
											}
										} else {
											echo "0 results";
										}
										  ?>
										</select> 
                                        
                                </div>
							<div class="form-group">
                                    <label class="">Designer</label>
                                        <input class="form-control disabled" type="text" name="designer" id="designer" disabled class="validate[required] form-control" value="<?php echo $_SESSION['user']; ?>">
                                </div>
                                <div class="form-group">
                                    <label class="">Depth</label>
                                        <input type="number" name="depth" id="depth" class="validate[required] form-control" placeholder="0">
                                </div>
								<div class="form-group">
                                    <label class="">Length</label>
                                        <input type="number" name="length" id="length" class="validate[required] form-control" placeholder="0">
                                </div>
								<div class="form-group">
                                    <label class="">Width</label>
                                        <input type="number" name="width" id="width" class="validate[required] form-control" placeholder="0">
                                </div>
								<button class="btn btn-primary btn-block" value="" type="submit">Submit</button>
                                                            </form>
															
															<!---- Simply a form to input into the actual PHP code, this gets thrown onto the overlay on the home page --->