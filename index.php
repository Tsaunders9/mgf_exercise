<head>
<?php
include "common/loggedin.php";
include "common/common.php";
// These handle the logins/session variables and store all links to stuff like css/jquery
?>
</head>
<body>
<title>
Dashboard
</title>
<!--- This overlay won't appear at first but will be filled by relevent content later using ajax --->
<div id="easy-overlay">
<form id="overlay-form" class="validate-form" method="post" action="adddesign.php">
                                <div class="form-group">
                                    <label class="">Design Title</label>
                                    <div>
                                        <input name="title" class="validate[required] form-control" placeholder="Everything has proceded according to my design.">
                                    </div>
                                </div>
								<div class="form-group">
                                    <label class="">Design Type</label>
									
									 <select name="type">
									 <?php
									 
									 include "common/databasedeets.php";
									 
									 	$sql = "SELECT * FROM design_types";
										$result = $conn->query($sql);
										
										if ($result->num_rows > 0) {
										
											while($row = $result->fetch_assoc()) {
												$type = $row["id"];
												$desc = $row["description"];
												echo "<option value='$type'>$desc</option>";
											}
										} else {
											echo "0 results";
										}
										  ?>
										</select> 
                                        
                                </div>
							<div class="form-group">
                                    <label class="">Designer</label>
                                        <input class="form-control disabled" type="text" name="designer" id="designer" disabled class="validate[required] form-control" value="<?php echo $_SESSION['user']; ?>">
                                </div>
                                <div class="form-group">
                                    <label class="">Depth</label>
                                        <input type="number" name="depth" id="depth" class="validate[required] form-control" placeholder="0">
                                </div>
								<div class="form-group">
                                    <label class="">Length</label>
                                        <input type="number" name="length" id="length" class="validate[required] form-control" placeholder="0">
                                </div>
								<div class="form-group">
                                    <label class="">Width</label>
                                        <input type="number" name="width" id="width" class="validate[required] form-control" placeholder="0">
                                </div>
								<button class="btn btn-primary btn-block" value="" type="submit">Submit</button>
                                                            </form>
</div>
	<?php
/*	include "common/sidebar.php"; */
	include "common/topbar.php";
	//Sidebar was removed when I decided I was getting way too complicated, topbar simply slots at the top and states the logged in users name
	?>
		<div class="panel">
			<h2>
			Designs
			</h2>
			<button class="add-new" href="">Add New Design</button>
				<div class="main-stuff">
				<?php
				include "common/databasedeets.php";
	
						$username = $conn->real_escape_string($_SESSION['user']);


							if($_SESSION['role'] == "senior"){
								$sql = "SELECT *, designs.title as dtitle, designs.id as mainid FROM designs 
								INNER JOIN design_senior_engineers ON designs.customer_id = design_senior_engineers.id
								LEFT JOIN design_types ON designs.design_type_id = design_types.id";
	//	$sql = "SELECT * FROM design_senior_engineers WHERE userid = '$username' LIMIT 1";
	}
	else if($_SESSION['role'] == "engineer"){
		$sql = "SELECT *, designs.title as dtitle, designs.id as mainid FROM designs 
		INNER JOIN design_engineers ON designs.customer_id = design_engineers.id
		LEFT JOIN design_types ON designs.design_type_id = design_types.id";
	}
	// Users are in different tables, so using sql differently based on session variables
	// Using an inner join so I can steal the name of the person for our table rows
						$result = $conn->query($sql);
						
						if ($result->num_rows > 0) {
							echo "<table class='main-table'>";
										echo "<tr class='header-row row'>
										<th class='table-text'>Title</th>
										<th class='table-text'>Design Type</th>
										<th class='table-numbers'>Depth</th>
										<th class='table-numbers'>Length</th>
										<th class='table-numbers'>Width</th>
										<th class='table-text'>Added By</th>
										<th class='table-text'>Design an Issue</th>
										<th class='ids'>ID</th>";
										if($_SESSION['role'] == "senior"){
											echo "<th class='table-text'>View Issues</th>
											<th class='table-text'>Delete Design</th>";
										}
										// Simply the headers for our designs table - if theyre a senior they get access to the issue viewing and the ability to delete
										echo "</tr>";
											while($row = $result->fetch_assoc()) {
												$title = $row["dtitle"];
												// Needed to do an as due to multiple fields being called 'title'
												$depth = $row["depth"];
												$length = $row["length"];
												$width = $row["width"];
												
												$name = $row["name"];
												
												$description = $row["description"];
												
												$designid = $row["mainid"];
												//Same as last, multiple 'id' so i renamed it in the SQL
												echo "<tr class='row'>
												<td class='table-text'>$title</td>
												<td class='table-text'>$description</td>
												<td class='table-numbers'>$depth</td>
												<td class='table-numbers'>$length</td>
												<td class='table-numbers'>$width</td>
												<td class='table-text'>$name</td>
												<td class='table-text design-issue'><i class='fa fa-plus-circle'></i></td>
												<td class='table-text ids main-id'>$designid</td>";
												if($_SESSION['role'] == "senior"){
													echo 
													"<td class='table-text view-issues'><i class='fa fa-clipboard'></i></th>
													<td class='table-text delete-stuff'><i class='fa fa-times-circle'></i></th>";
												}
												//Same as the last but fr the content, taken out of the big SQL statement earlier
												echo "</tr>";
												/* Going to make more rows to make an expandible mini table, but only if its a senior */
												if($_SESSION['role'] == "senior"){
												$sql2 = "SELECT
															  design_issues.id,
															  design_issues.description,
															  design_issues.date_in,
															  design_issues.date_out,
															  design_senior_engineers.userid,
															  design_senior_engineers.name,
															  design_engineers.name AS normid,
															  Checker_senior.name AS seniorchecker,
															  Checker_engineer.name AS normcheck,
															  design_statuses.description AS status
															FROM
															  design_issues
															  LEFT JOIN design_statuses ON design_issues.status_id = design_statuses.id
															  AND design_id = '$designid' 
															  LEFT JOIN design_senior_engineers ON design_senior_engineers.id = design_issues.designer_id
															  AND design_id = '$designid' 
															  LEFT JOIN design_engineers ON design_engineers.id = design_issues.designer_id
															  AND design_id = '$designid' 
															  LEFT JOIN design_senior_engineers AS Checker_senior ON Checker_senior.id = design_issues.checker_id
															  AND design_id = '$designid' AND design_issues.checker_id IS NOT NULL
															  LEFT JOIN design_engineers AS Checker_engineer ON Checker_engineer.id = design_issues.checker_id
															  AND design_id = '$designid' AND design_issues.checker_id IS NOT NULL
															  GROUP BY design_issues.id";
												$result2 = $conn->query($sql2);
													// Alot more spegetti then I wouldve liked, simply grabs the design issue rows that are needed, and uses the more user friendly parts of others "Bart Simpson" for example instead of 10002
													if ($result2->num_rows > 0) {
														echo "<tr class='inner-rows-header inner-rows'>
														<td>Issue Number</td>
															<td>Description</td>
															<td>Date In</td>
															<td>Date Out</td>
															<td>Designer</td>
															<td>Checker</td>
															<td>Status</td>
															<td>Check Issue</td>
															</tr>";
														while($row2 = $result2->fetch_assoc()) {
															
															$issueno = $row2["id"];
															$description2 = $row2["description"];
															$date_in = $row2["date_in"];
															$date_out = $row2["date_out"];
															
															if($row2["normid"] == null){
																$design = $row2["name"];
															}
															else{
																$design = $row2["normid"];
															}
															if($row2["seniorchecker"] == null){
																$checker = $row2["normcheck"];
															}
															else{
																$checker = $row2["seniorchecker"];
															}
															$status = $row2["status"];
															
															$desus = $row2["userid"];
															
															
															echo "<tr class='inner-rows'>
															<td class='issue-no'>$issueno</td>
															<td>$description2</td>
															<td>$date_in</td>
															<td>$date_out</td>
															<td>$design</td>
															<td>$checker</td>
															<td>$status</td>";
															if($_SESSION['user'] != $desus){
															echo "
															<td class='check-issue'>
															<i class='fa fa-check'></i></td>
															
															</tr>";
															//Makes a bunch of issues using that SQL from earlier, theyre hidden by default to prevent clutter
															}
															else{
																echo "<td>&nbsp;</td>";
															}
														}
														
													}
												
												}
												
												
											}
										} else {
											echo "No Designs are in the database - please create one now.";
										}
				
				
				?>

				</div>
		</div>
		<script>
		$(document).ready(function () {
			
			//All our jquery functions - they operate similarly by taking a PHP file and putting its output into our overlay from the start, this then appears over everything and the user can fill out a form.
					$( ".add-new" ).click(function() {
			var iter = $('.add-new').index(this);
			
			var inputurl = "adddesignform.php";
  $("#easy-overlay").load(inputurl); 
  document.getElementById("easy-overlay").style.display = "block";
});

		$( ".design-issue" ).click(function() {
			var iter = $('.design-issue').index(this);
			
			var designid = document.getElementsByClassName("main-id")[iter].innerHTML;
			var inputurl = "addissueform.php?desin="+designid;
  $("#easy-overlay").load(inputurl); 
  document.getElementById("easy-overlay").style.display = "block";
});
		$( ".check-issue" ).click(function() {
			var iter = $('.check-issue').index(this);
			
			var issueno = document.getElementsByClassName("issue-no")[iter].innerHTML;
			var inputurl = "checkissueform.php?issueno="+issueno;
  $("#easy-overlay").load(inputurl); 
  document.getElementById("easy-overlay").style.display = "block";
});
		$( ".view-issues" ).click(function() {
			
			var issueno = document.getElementsByClassName("inner-rows");
			for(i=0;i<issueno.length;i++){
				issueno[i].style.display = "table-row";
			}
		});
		$( ".delete-stuff" ).click(function() {
			var iter = $('.delete-stuff').index(this);
			
			var designid = document.getElementsByClassName("main-id")[iter].innerHTML;
			
			var deleteurl = "delete.php?desid="+designid
			window.location.href = deleteurl;
			
			// Didn't want to needlessly complicate it here for a coding exercise, simply sends user to a fast bit of PHP since theres so few variables
		});
		});
		</script>
</body>